const {HelloRequest, HelloReply, RepeatHelloRequest} = require('./helloworld_pb.js');
const {GreeterClient} = require('./helloworld_grpc_web_pb.js');

var client = new GreeterClient('http://localhost:8080');

var request = new HelloRequest();
request.setName('World');

console.log("sayHello request");
console.log(request);

client.sayHello(request, {}, (err, response) => {
  if (err) {
    console.error(err);
  } else {
    console.log(response.getMessage());
  }
});

console.log("sent sayHello");
console.log(RepeatHelloRequest)

var streamRequest = new RepeatHelloRequest();
streamRequest.setName('World');
streamRequest.setCount(5);

console.log("sayRepeatHello request");
console.log(streamRequest);

var stream = client.sayRepeatHello(streamRequest, {});
stream.on('data', (response) => {
  console.log(response.getMessage());
});

console.log("sent sayRepeatHello");

var deadline = new Date();
deadline.setSeconds(deadline.getSeconds() + 1);

client.sayHelloAfterDelay(request, {deadline: deadline.getTime()},
  (err, response) => {
    console.log('Got error, code = ' + err.code + ', message = ' + err.message);
  });
